module memorygame

go 1.15

require (
	github.com/gin-gonic/gin v1.7.1 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/mattn/go-sqlite3 v1.14.7 // indirect
	github.com/zhexuany/wordGenerator v0.0.0-20161102120352-1f13e790d534 // indirect
)
