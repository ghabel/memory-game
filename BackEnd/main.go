package main

import (
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/mattn/go-sqlite3"
)

type Assets struct {
	ID        int    `gorm:"AUTO_INCREMENT" form:"id" json:"id"`
	Img string `gorm:"not null" form:"img" json:"img"`
	Similar bool `gorm:"not null" form:"similar" json:"similar"`
	Open bool `gorm:"not null" form:"open" json:"open"`
}

type Users struct {
	ID        int    `gorm:"AUTO_INCREMENT" form:"id" json:"id"`
	Username string `gorm:"not null" form:"username" json:"username"`
	Score  int64 `gorm:"default 0" form:"score" json:"score"`
}

func InitDb() *gorm.DB {
	// Openning file
	db, err := gorm.Open("sqlite3", "./data.db")
	// Display SQL queries
	db.LogMode(true)

	// Error
	if err != nil {
		panic(err)
	}
	// Creating the table
	if !db.HasTable(&Users{}) {
		db.CreateTable(&Users{})
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&Users{})
	}
	if !db.HasTable(&Assets{}) {
		db.CreateTable(&Assets{})
		db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(&Assets{})
	}

	return db
}



//Cors is the additional header on every request to server
func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Writer.Header().Add("Access-Control-Allow-Origin", "*")
		c.Next()
	}
}

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	r.Use(Cors())

	v1 := r.Group("api/v1")
	{
		v1.POST("/users", PostUser)
		v1.GET("/users", GetUsers)
		v1.GET("/assets", GetAssets)
		v1.GET("/users/:id", GetUser)
		v1.POST("/users/:id", UpdateUser)
	}
	MockAssets()
	r.Run(":3000")
}


func PostUser(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var user Users
	c.Bind(&user)


	if user.Username != ""  {
		db.Create(&user)
		c.JSON(201, gin.H{"success": user})
	} else {
		c.JSON(422, gin.H{"error": "Fields are empty"})
	}

}


func GetUsers(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var users []Users
	db.Find(&users)

	c.JSON(200, users)

}
func GetAssets(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	var assets []Assets
	db.Find(&assets)

	c.JSON(200, assets)

}

func GetUser(c *gin.Context) {

	db := InitDb()

	defer db.Close()

	id := c.Params.ByName("id")
	var user Users

	db.First(&user, id)

	if user.ID != 0 {

		c.JSON(200, user)
	} else {

		c.JSON(404, gin.H{"error": "User not found"})
	}
}

func UpdateUser(c *gin.Context) {
	db := InitDb()
	defer db.Close()

	id := c.Params.ByName("id")
	var user Users
	db.First(&user, id)

	if user.Score != 1000003322332  {

		if user.ID != 0 {
			var newUser Users
			c.Bind(&newUser)

			result := Users{
				ID:  user.ID,
				Username: user.Username,
				Score:  newUser.Score,
			}

			db.Save(&result)
			c.JSON(200, gin.H{"success": result})
		} else {
			c.JSON(404, gin.H{"error": "User not found"})
		}

	} else {
		c.JSON(422, gin.H{"error": "Fields are empty"})
	}
}


func MockAssets() {
	db := InitDb()
	defer db.Close()

	var photo[16]string

	photo[0]="GenZ.jpg"
	photo[1]="JustCause3.jpg"
	photo[2]="JustCause4.jpg"
	photo[3]="MadMax.jpg"
	photo[4]="Rage2.jpg"
	photo[5]="SecondExtinction.jpg"
	photo[6]="theHunter.jpg"
	photo[7]="TheHunter2.jpg"
	photo[8]="GenZ.jpg"
	photo[9]="JustCause3.jpg"
	photo[10]="JustCause4.jpg"
	photo[11]="MadMax.jpg"
	photo[12]="Rage2.jpg"
	photo[13]="SecondExtinction.jpg"
	photo[14]="theHunter.jpg"
	photo[15]="TheHunter2.jpg"

	for _, s := range photo {
		var asset Assets
		asset.Img = s
		asset.Open = false
		asset.Similar = false
		db.Create(&asset)
	}

}

